/**
 * <pre>
 * Copyright (C) 2011 Ferry Huberts
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 * </pre>
 */
package nl.pelagic.osgi.jnotify.demo;

import java.io.File;

import net.contentobjects.jnotify.JNotify;
import net.contentobjects.jnotify.JNotifyException;
import net.contentobjects.jnotify.JNotifyListener;
import aQute.bnd.annotation.component.Activate;
import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Deactivate;
import aQute.bnd.annotation.component.Reference;

@Component(immediate = true, provide = {})
public class JNotifyDemo implements JNotifyListener {

	private JNotify notifyService = null;

	private String watchPaths[] = {"/home/ferry", "/home/ferry/Downloads"};
	private int masks[] = {JNotify.FILE_ANY, JNotify.FILE_ANY};
	private int wds[] = {-1, -1};

	private synchronized void doWatch(boolean add) throws JNotifyException {
		System.out.println(this.getClass().getSimpleName() + ": doWatch(" + Boolean.valueOf(add) + ")");
		for (int i = 0; i < wds.length; i++) {
			if (add) {
				if (wds[i] != -1) {
					return;
				}
				if (this.notifyService == null) {
					System.out.println(this.getClass().getSimpleName() + ": Not setting up watch: no notifyService available");
					return;
				}
				wds[i] = this.notifyService.addWatch(watchPaths[i], masks[i], false, this);
				System.out.println(this.getClass().getSimpleName() + ": Setup watch, wd[" + i + "]=" + wds[i]);
			} else {
				if (wds[i] == -1) {
					return;
				}
				if (this.notifyService == null) {
					System.out.println(this.getClass().getSimpleName() + ": Not removing watch: no notifyService available");
					return;
				}
				try {
					System.out.println(this.getClass().getSimpleName() + ": Remove watch, wd[" + i + "]=" + wds[i]);
					this.notifyService.removeWatch(wds[i]);
				} catch (JNotifyException e) {
					e.printStackTrace();
				}
				wds[i] = -1;
			}
		}
	}

	@Reference(optional = false, dynamic = true, multiple = false)
	public synchronized void setNotifyService(JNotify notifyService) throws JNotifyException {
		System.out.println(this.getClass().getSimpleName() + ": Set notify service");
		this.notifyService = notifyService;
		doWatch(true);
	}

	public synchronized void unsetNotifyService(@SuppressWarnings("hiding") JNotify notifyService)
			throws JNotifyException {
		if (this.notifyService == notifyService) {
			System.out.println(this.getClass().getSimpleName() + ": Unset notify service");
			doWatch(false);
			this.notifyService = null;
		}
	}

	@Activate
	public synchronized void activate() throws JNotifyException {
		System.out.println(this.getClass().getSimpleName() + ": Activate");
		doWatch(true);
	}

	@Deactivate
	public synchronized void deactivate() throws JNotifyException {
		System.out.println(this.getClass().getSimpleName() + ": Deactivate");
		doWatch(false);
	}

	private String constructName(String rootPath, String name) {
		if ((name == null) || name.isEmpty()) {
			return rootPath;
		}

		return rootPath + File.separator + name;
	}

	@Override
	public void fileCreated(int wd, String rootPath, String name) {
		System.out.println(this.getClass().getSimpleName() + ": Created: " + constructName(rootPath, name));
	}

	@Override
	public void fileDeleted(int wd, String rootPath, String name) {
		System.out.println(this.getClass().getSimpleName() + ": Deleted: " + constructName(rootPath, name));
	}

	@Override
	public void fileModified(int wd, String rootPath, String name) {
		System.out.println(this.getClass().getSimpleName() + ": Updated: " + constructName(rootPath, name));
	}

	@Override
	public void fileRenamed(int wd, String rootPath, String oldName, String newName) {
		System.out.println(this.getClass().getSimpleName() + ": Renamed: " + ((oldName != null) ? oldName : "") + " -> "
				+ constructName(rootPath, newName));
	}
}