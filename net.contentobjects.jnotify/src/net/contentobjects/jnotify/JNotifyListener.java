/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 * 
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * Copyright (c) 2011 - Ferry Huberts
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 * 
 ******************************************************************************
 * Authors : Omry Yadan
 *           Ferry Huberts, 2011
 * </pre>
 ******************************************************************************/

package net.contentobjects.jnotify;

/**
 * Callback interface for a JNotify client
 */
public interface JNotifyListener {
	/**
	 * Called when a file is created
	 * 
	 * @param wd the watch handle
	 * @param rootPath the directory of the watch
	 * @param name the name of the file that is created
	 */
	public void fileCreated(int wd, String rootPath, String name);

	/**
	 * Called when a file is deleted
	 * 
	 * @param wd the watch handle
	 * @param rootPath the directory of the watch
	 * @param name the name of the file that is deleted
	 */
	public void fileDeleted(int wd, String rootPath, String name);

	/**
	 * Called when a file is modified
	 * 
	 * @param wd the watch handle
	 * @param rootPath the directory of the watch
	 * @param name the name of the file that is modified
	 */
	public void fileModified(int wd, String rootPath, String name);

	/**
	 * Called when a file is renamed. Note that this function is NOT called when
	 * an entry is moved from a watch to a non-watch. Also, the function IS called
	 * when an entry is moved from a non-watch to a watch, in which case the
	 * oldName will be null.
	 * 
	 * @param wd the watch handle
	 * @param rootPath the directory of the watch
	 * @param oldName the old name of the file
	 * @param newName the new name of the file
	 */
	public void fileRenamed(int wd, String rootPath, String oldName, String newName);
}
