/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 * 
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * Copyright (c) 2011 - Ferry Huberts
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 * 
 ******************************************************************************
 * Authors : Omry Yadan
 *           Ferry Huberts, 2011
 * </pre>
 ******************************************************************************/

package net.contentobjects.jnotify;

/**
 * JNotify Service Interface
 */
public interface JNotify {
	/** event for file creation events */
	public static final int FILE_CREATED = 0x01;

	/** event for file deletion events */
	public static final int FILE_DELETED = 0x02;

	/** event for file modification events */
	public static final int FILE_MODIFIED = 0x04;

	/** event for file rename events */
	public static final int FILE_RENAMED = 0x08;

	/** event for all file events */
	public static final int FILE_ANY = FILE_CREATED | FILE_DELETED | FILE_MODIFIED | FILE_RENAMED;

	/**
	 * Add a watch.
	 * 
	 * @param path the path to watch, either a file or directory
	 * @param mask the event mask, see the FILE_... defines
	 * @param watchSubtree true to watch all sub-directories (performance impact!)
	 * @param listener the listener that is to consume the events
	 * @return the watch handle (-1 on error)
	 * @throws JNotifyException upon error
	 */
	public int addWatch(String path, int mask, boolean watchSubtree, JNotifyListener listener) throws JNotifyException;

	/**
	 * Remove a watch
	 * 
	 * @param wd the watch handle
	 * @return true when successfully removed, false otherwise
	 * @throws JNotifyException upon error
	 */
	public boolean removeWatch(int wd) throws JNotifyException;;
}
