/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 * 
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * Copyright (c) 2011 - Ferry Huberts
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 * 
 ******************************************************************************
 * Authors : Omry Yadan
 *           Ferry Huberts, 2011
 * </pre>
 ******************************************************************************/

package net.contentobjects.jnotify.demo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.contentobjects.jnotify.JNotify;
import net.contentobjects.jnotify.JNotifyException;
import net.contentobjects.jnotify.JNotifyListener;
import net.contentobjects.jnotify.impl.JNotifyImpl;

/**
 * Demo
 */
public class Demo {
	/** sync object */
	static final Object sync = new Object();

	private static void sync() {
		synchronized (sync) {
			sync.notifyAll();
		}
	}

	private static void deactivate(JNotifyImpl impl, int fd) {
		if (fd != -1) {
			System.out.println("wd " + fd + ": Removing watch");
			try {
				impl.removeWatch(fd);
			} catch (JNotifyException e) {
				e.printStackTrace();
			}
			fd = -1;
		}

		System.out.println("Deactivating JNotify");
		impl.deactivate();
	}

	/**
	 * Main
	 * 
	 * @param args arguments
	 * @throws IOException upon error
	 */
	public static void main(String[] args) throws IOException {
		Level logLevel = Level.INFO;

		Logger logger = Logger.getLogger("net.contentobjects.jnotify");
		ConsoleHandler ch = new ConsoleHandler();
		logger.addHandler(ch);
		for (Handler handler : logger.getHandlers()) {
			handler.setLevel(logLevel);
		}
		logger.setLevel(logLevel);

		int fd = -1;
		final String prefix = "  *** LISTENER: ";

		String dir = new File(args.length == 0 ? "." : args[0]).getCanonicalFile().getAbsolutePath();
		JNotifyImpl impl = new JNotifyImpl();
		File curDir = new File(".");

		System.out.println("Activating JNotify");
		try {
			impl.activate();
		} catch (JNotifyException e) {
			e.printStackTrace();
			return;
		}

		try {
			fd = impl.addWatch(dir, JNotify.FILE_ANY, false, new JNotifyListener() {
				@Override
				public void fileCreated(int wd, String rootPath, String name) {
					System.out.println(prefix + "wd " + wd + ": created " + rootPath + File.separator + name);
					sync();
				}

				@Override
				public void fileModified(int wd, String rootPath, String name) {
					System.out.println(prefix + "wd " + wd + ": modified " + rootPath + File.separator + name);
					sync();
				}

				@Override
				public void fileDeleted(int wd, String rootPath, String name) {
					System.out.println(prefix + "wd " + wd + ": deleted " + rootPath + File.separator + name);
					sync();
				}

				@Override
				public void fileRenamed(int wd, String rootPath, String oldName, String newName) {
					System.out.println(prefix + "wd " + wd + ": renamed " + oldName + " -> " + rootPath + File.separator
							+ newName);
					sync();
				}
			});
		} catch (JNotifyException e) {
			System.out.println("Could not add watch");
			deactivate(impl, fd);
			return;
		}
		System.out.println("wd " + fd + ": Watching directory " + curDir.getCanonicalPath());

		try {
			System.out.println("Creating a temporary file");
			File tmpFile1;
			synchronized (sync) {
				tmpFile1 = File.createTempFile("demo", null, curDir);
				sync.wait(500);
			}

			System.out.println("Creating another temporary file");
			File tmpFile2;
			synchronized (sync) {
				tmpFile2 = File.createTempFile("demo", null, curDir);
				sync.wait(500);
			}

			System.out.println("Writing to " + tmpFile1.getName());
			synchronized (sync) {
				FileWriter fw = new FileWriter(tmpFile1);
				fw.write("Writing to the temporary file");
				fw.close();
				sync.wait(500);
			}

			System.out.println("Renaming  " + tmpFile1.getName() + " to " + tmpFile2.getName());
			synchronized (sync) {
				if (!tmpFile1.renameTo(tmpFile2)) {
					System.out.println("Rename failed");
				} else {
					sync.wait(500);
				}
			}

			System.out.println("Deleting " + tmpFile2.getName());
			synchronized (sync) {
				if (!tmpFile2.delete()) {
					System.out.println("Delete failed");
				} else {
					sync.wait(500);
				}
			}
		} catch (InterruptedException e) {
			System.out.println("Interrupted");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO Exception");
			e.printStackTrace();
		} finally {
			deactivate(impl, fd);
		}

		System.out.println("End");
	}
}
