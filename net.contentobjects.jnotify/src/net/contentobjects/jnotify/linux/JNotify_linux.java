/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 * 
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * Copyright (c) 2011 - Ferry Huberts
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 * 
 ******************************************************************************
 * Authors : Omry Yadan
 *           Ferry Huberts, 2011
 * </pre>
 ******************************************************************************/

package net.contentobjects.jnotify.linux;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.contentobjects.jnotify.JNotifyException;

/**
 * This class is the interface towards the native code
 */
class JNotify_linux {
	private Logger logger = Logger.getLogger(this.getClass().getName());

	static {
		System.loadLibrary("jnotify");
	}

	/*
	 * Native methods
	 */

	/**
	 * Signals the native code to stop.
	 * 
	 * @return errno
	 */
	private native int nativeStop();

	/**
	 * Instruct the native code to add a watch
	 * 
	 * @param path the file
	 * @param mask the mask of events to watch for
	 * @return -errno on error, the watch descriptor (&gt; 0) otherwise
	 */
	private native int nativeAddWatch(String path, int mask);

	/**
	 * Instruct the native code to remove a watch
	 * 
	 * @param wd the watch descriptor
	 * @return errno
	 */
	private native int nativeRemoveWatch(int wd);

	/**
	 * Runs the native event loop. Does not return, so this method must be called
	 * in a separate thread!
	 * 
	 * @return errno
	 */
	private native int nativeNotifyLoop();

	/**
	 * Convert errno into a string: calls native strerror(errno)
	 * 
	 * @param errno errno
	 * @return the string representation
	 */
	private native String getStrError(int errno);

	/*
	 * Access methods
	 */

	/**
	 * Create a new watch
	 * 
	 * @param path the path of the file to watch
	 * @param mask the mask with events to watch for
	 * @return the watch descriptor
	 * @throws JNotifyException upon error
	 */
	int addWatch(String path, int mask) throws JNotifyException {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					new Object[] {path, Events.getMaskDesc(mask)});
		}

		if (thread == null) {
			throw new IllegalStateException("Not started");
		}

		int wd = nativeAddWatch(path, mask);

		if (wd < 0) {
			throw new JNotifyException_linux("Error watching " + path + " : " + getStrError(-wd), -wd);
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					Integer.valueOf(wd));
		}

		return wd;
	}

	/**
	 * Remove a watch
	 * 
	 * @param wd the file descriptor of the watch that must be removed
	 * @throws JNotifyException upon error
	 */
	void removeWatch(int wd) throws JNotifyException {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					new Object[] {Integer.valueOf(wd)});
		}

		if (thread == null) {
			throw new IllegalStateException("Not started");
		}

		int ret = nativeRemoveWatch(wd);

		if (ret != 0) {
			throw new JNotifyException_linux("Error removing watch " + wd + " : " + getStrError(ret), ret);
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/** the thread in which the native runloop runs */
	private Thread thread = null;

	/** the listener to notify of watch events */
	private INotifyListener _notifyListener;

	/** the return code of the native runloop */
	private int runloopResult = 0;

	/**
	 * Start the watch system
	 * 
	 * @param listener the listener to notify of watch events (must be non null)
	 * @throws JNotifyException upon error
	 */
	void start(INotifyListener listener) throws JNotifyException {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(), listener);
		}

		if (thread != null) {
			throw new IllegalStateException("Already started");
		}

		if (listener == null) {
			throw new IllegalArgumentException("Must provide a listener");
		}

		Thread threadLocal = new Thread() {
			@Override
			public void run() {
				runloopResult = nativeNotifyLoop();
				if (runloopResult != 0) {
					logger.log(Level.SEVERE, "Native loop exited with error: " + getStrError(runloopResult));
				}
			}
		};
		threadLocal.setName("INotify (" + threadLocal.getId() + ")");
		threadLocal.setDaemon(true);

		threadLocal.start();

		synchronized (_running) {
			try {
				_running.wait();
			} catch (InterruptedException e) {
				/* swallow */
			}
		}

		if (!_running.get()) {
			throw new JNotifyException_linux("Failed to start native runloop: " + getStrError(runloopResult), runloopResult);
		}

		_notifyListener = listener;
		thread = threadLocal;

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/**
	 * Stop the watch system
	 */
	void stop() {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}

		if (thread == null) {
			throw new IllegalStateException("Not started");
		}

		int res = nativeStop();
		if (res != 0) {
			logger.log(Level.SEVERE, "Error stopping inotify library: " + getStrError(res));
		} else {
			try {
				thread.join();
			} catch (InterruptedException e) {
				/* swallow */
			}
		}

		_notifyListener = null;
		thread = null;

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/**
	 * Callback for the native code, called when one or more watch events are
	 * available
	 * 
	 * @param name the filename that caused the events
	 * @param wd the watch descriptor that evoked the events
	 * @param mask the mask of events
	 * @param cookie a unique cookie for correlating events (moves)
	 */
	private void callbackProcessEvent(String name, int wd, int mask, int cookie) {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					new Object[] {name, Integer.valueOf(wd), Events.getMaskDesc(mask), Integer.valueOf(cookie)});
		}

		if (_notifyListener != null) {
			_notifyListener.notify(name, wd, mask, cookie);
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/** true when the native runloop is running */
	private AtomicBoolean _running = new AtomicBoolean(false);

	/**
	 * Callback for the native code, called when the runloop needs to signal a
	 * change in running state
	 * 
	 * @param running true when the native runloop is running
	 */
	private void callbackRunning(boolean running) {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					Boolean.valueOf(running));
		}

		synchronized (_running) {
			_running.set(running);
			_running.notifyAll();
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}
}
