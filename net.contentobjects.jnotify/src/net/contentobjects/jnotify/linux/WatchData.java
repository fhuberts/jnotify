/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 * 
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * Copyright (c) 2011 - Ferry Huberts
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 * 
 ******************************************************************************
 * Authors : Omry Yadan
 *           Ferry Huberts, 2011
 * </pre>
 ******************************************************************************/

package net.contentobjects.jnotify.linux;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.contentobjects.jnotify.JNotifyListener;

/**
 * Container for watch data
 */
class WatchData {
	private Logger logger = Logger.getLogger(this.getClass().getName());

	/** true when the watch is created by the user / is a root watch */
	boolean _user;

	/** the watch descriptor */
	int _linuxWd;

	/** a list sub-watches, created by subtree watching */
	List<Integer> _subWd;

	/** the JNotify event mask */
	int _mask;

	/** the Linux event mask */
	int _linuxMask;

	/** true to do subtree watching */
	boolean _watchSubtree;

	/** the listener that receives event notifications */
	JNotifyListener _listener;

	/** the file path of the watch */
	String _path;

	/** the parent watch data, non-null under subtree watching */
	WatchData _parentWatchData;

	/**
	 * Constructor
	 * 
	 * @param parentWatchData the parent watch data, non-null under subtree
	 *        watching
	 * @param user true when the watch is created by the user / is a root watch
	 * @param path the file path of the watch
	 * @param linuxWd the watch descriptor
	 * @param mask the JNotify event mask
	 * @param linuxMask the Linux event mask
	 * @param watchSubtree true to do subtree watching
	 * @param listener the listener that receives event notifications
	 */
	WatchData(WatchData parentWatchData, boolean user, String path, int linuxWd, int mask, int linuxMask,
			boolean watchSubtree, JNotifyListener listener) {
		if (listener == null) {
			throw new IllegalArgumentException("Need to specify a listener");
		}

		_user = user;
		_linuxWd = linuxWd;
		/* _subWd is lazily initialized */
		_mask = mask;
		_linuxMask = linuxMask;
		_watchSubtree = watchSubtree;
		_listener = listener;
		_path = path;
		_parentWatchData = parentWatchData;
	}

	/**
	 * Notify the configured listener of a rename event
	 * 
	 * @param name the file that triggered the event
	 * @param rc the rename container
	 */
	void notifyFileRenamed(String name, RenamingContainer rc) {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					new Object[] {name, rc});
		}

		String oldName = null;

		if (rc != null) {
			String inRoot = getOutRoot(rc.wd);
			String inOldName = getOutName(rc.wd, rc.name);
			oldName = inRoot;
			if (!inOldName.isEmpty()) {
				oldName += File.separator + inOldName;
			}
		}

		String outRoot = getOutRoot(this);
		String outNewName = getOutName(this, name);
		_listener.fileRenamed(getParentWD(), outRoot, oldName, outNewName);

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/**
	 * Notify the configured listener of a modification event
	 * 
	 * @param name the file that triggered the event
	 */
	void notifyFileModified(String name) {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(), name);
		}

		String outRoot = getOutRoot(this);
		String outName = getOutName(this, name);
		_listener.fileModified(getParentWD(), outRoot, outName);

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/**
	 * Notify the configured listener of a deletion event
	 * 
	 * @param name the file that triggered the event
	 */
	void notifyFileDeleted(String name) {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(), name);
		}

		String outRoot = getOutRoot(this);
		String outName = getOutName(this, name);
		_listener.fileDeleted(getParentWD(), outRoot, outName);

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/**
	 * Notify the configured listener of a creation event
	 * 
	 * @param name the file that triggered the event
	 */
	void notifyFileCreated(String name) {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(), name);
		}

		String outRoot = getOutRoot(this);
		String outName = getOutName(this, name);
		_listener.fileCreated(getParentWD(), outRoot, outName);

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/**
	 * Remove a sub watch
	 * 
	 * @param linuxWd the watch descriptor
	 */
	void removeSubwatch(int linuxWd) {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					Integer.valueOf(linuxWd));
		}

		if (_subWd == null) {
			throw new RuntimeException("Can't remove subwatch " + linuxWd + ", none are present");
		}

		if (!_subWd.remove(Integer.valueOf(linuxWd))) {
			throw new RuntimeException("Error removing " + linuxWd + " from list");
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/**
	 * Add a sub watch
	 * 
	 * @param linuxWd the watch descriptor
	 */
	void addSubwatch(int linuxWd) {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					Integer.valueOf(linuxWd));
		}

		if (_subWd == null) {
			_subWd = new ArrayList<Integer>();
		}
		_subWd.add(Integer.valueOf(linuxWd));

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/**
	 * @param wd the watch descriptor
	 * @return the parent watch data
	 */
	static WatchData getParentWatch(WatchData wd) {
		return wd._user ? wd : wd._parentWatchData;
	}

	/**
	 * @param wd the watch data
	 * @return the effective path of the watch
	 */
	private String getOutRoot(WatchData wd) {
		String outRoot;
		if (wd._user) {
			outRoot = wd._path;
		} else {
			outRoot = getParentWatch(wd)._path;
		}
		return outRoot;

	}

	/**
	 * @param wd the watch data
	 * @param name the file that triggered the event
	 * @return the effective path for an event
	 */
	private String getOutName(WatchData wd, String name) {
		String outName;
		if (wd._user) {
			outName = name;
		} else {
			outName = wd._path.substring(getParentWatch(wd)._path.length() + 1);
			if ((name != null) && !name.isEmpty()) {
				outName += File.separatorChar + name;
			}
		}
		return outName;
	}

	/**
	 * @return the parent watch descriptor when it exists, the current watch
	 *         descriptor otherwise
	 */
	private int getParentWD() {
		return _parentWatchData == null ? _linuxWd : _parentWatchData._linuxWd;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.getClass().getSimpleName() + " [_linuxWd=");
		builder.append(this._linuxWd);
		builder.append(", _path=");
		builder.append(this._path);
		builder.append(", _user=");
		builder.append(this._user);
		builder.append(", _watchSubtree=");
		builder.append(this._watchSubtree);
		builder.append(", _subWd=");
		builder.append(this._subWd);
		builder.append(", _mask=");
		builder.append(this._mask);
		builder.append(", _linuxMask=");
		builder.append(this._linuxMask);
		builder.append(", _parentWatchData=");
		builder.append((this._parentWatchData != null) ? Integer.valueOf(this._parentWatchData._linuxWd) : "");
		builder.append(", _listener=");
		builder.append(this._listener);
		builder.append("]");
		return builder.toString();
	}
}
