/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 * 
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * Copyright (c) 2011 - Ferry Huberts
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 * 
 ******************************************************************************
 * Authors : Omry Yadan
 *           Ferry Huberts, 2011
 * </pre>
 ******************************************************************************/

package net.contentobjects.jnotify.linux;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class handles the rename administration.
 */
class RenamingHandler {
	private Logger logger = Logger.getLogger(this.getClass().getName());

	/** a map of cookies against RenamingContainers, used for renaming */
	private Map<Integer, RenamingContainer> _cookieToOldName = new TreeMap<Integer, RenamingContainer>();

	/** the timer for the scrub task */
	private Timer scrubTimer = null;

	/** the scrub timer task */
	private TimerTask scrubTimerTask = null;

	/** the default scrub period in milliseconds */
	static final long SCRUB_PERIOD_DEFAULT = 1000;

	/** the scrub period in milliseconds */
	long scrubPeriod = SCRUB_PERIOD_DEFAULT;

	/**
	 * @param scrubPeriod the scrubPeriod to set
	 */
	public void setScrubPeriod(long scrubPeriod) {
		this.scrubPeriod = scrubPeriod;
		restartScrubTimerTask();
	}

	/**
	 * (Re)starts the scrub timer task: stops it first, creates a new task and
	 * starts that at a fixed rate of scrubPeriod
	 */
	private void restartScrubTimerTask() {
		if (scrubTimer == null) {
			return;
		}

		stopScrubTimerTask();

		scrubTimerTask = new TimerTask() {
			private Logger logger1 = Logger.getLogger(this.getClass().getName());

			@Override
			public void run() {
				/*
				 * This task scrubs the renaming map. This is needed because when a file
				 * is moved from a watched directory into a non-watched directory then
				 * an entry is created in the renaming map, but never removed, which is
				 * a leak.
				 */
				try {
					long currentTime = System.currentTimeMillis();
					long cutoffTime = currentTime - scrubPeriod;

					if (logger1.isLoggable(Level.FINER)) {
						logger1.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
								new Object[] {Long.valueOf(currentTime)});
					}

					Set<Integer> scrubCookies = new HashSet<Integer>();

					synchronized (_cookieToOldName) {
						for (Map.Entry<Integer, RenamingContainer> entry : _cookieToOldName.entrySet()) {
							RenamingContainer rc = entry.getValue();
							if (rc.time < cutoffTime) {
								scrubCookies.add(entry.getKey());
							}
						}
						for (Integer cookie : scrubCookies) {
							_cookieToOldName.remove(cookie);
						}
					}

					if (logger1.isLoggable(Level.FINER)) {
						logger1.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
					}
				} catch (Throwable e) {
					/*
					 * do don't want to let the timer task die because of spurious
					 * exceptions, but still log them
					 */
					logger1.log(Level.SEVERE, e.getMessage(), e);
				}
			}
		};

		scrubTimer.scheduleAtFixedRate(scrubTimerTask, scrubPeriod, scrubPeriod);
	}

	/**
	 * Stops the scrub timer task
	 */
	private void stopScrubTimerTask() {
		if (scrubTimerTask != null) {
			scrubTimerTask.cancel();
			scrubTimerTask = null;
		}
	}

	/**
	 * Add an event to the rename administration
	 * 
	 * @param cookie the cookie of the event
	 * @param rc the rename container
	 */
	void add(int cookie, RenamingContainer rc) {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					new Object[] {Integer.valueOf(cookie), rc});
		}

		synchronized (_cookieToOldName) {
			_cookieToOldName.put(Integer.valueOf(cookie), rc);
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/**
	 * Remove an event from the rename administration
	 * 
	 * @param cookie the cookie of the event
	 * @return the removed container (can be null)
	 */
	RenamingContainer remove(int cookie) {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					new Object[] {Integer.valueOf(cookie)});
		}

		synchronized (_cookieToOldName) {
			RenamingContainer rc = _cookieToOldName.remove(Integer.valueOf(cookie));

			if (logger.isLoggable(Level.FINER)) {
				logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(), rc);
			}

			return rc;
		}
	}

	/**
	 * Start the rename administration handler
	 */
	void start() {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}

		synchronized (_cookieToOldName) {
			_cookieToOldName.clear();
		}

		scrubTimer = new Timer(this.getClass().getSimpleName());
		restartScrubTimerTask();

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/**
	 * Stop the rename administration handler
	 */
	void stop() {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}

		stopScrubTimerTask();
		scrubTimer.cancel();
		scrubTimer = null;

		synchronized (_cookieToOldName) {
			_cookieToOldName.clear();
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}
}
