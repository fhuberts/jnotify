/**
 * <pre>
 * Copyright (C) 2011 Ferry Huberts
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 * </pre>
 */

package net.contentobjects.jnotify.linux;

/**
 * A container for rename information
 */
class RenamingContainer {
	/** the watch data */
	WatchData wd;

	/** the name that triggered the event */
	String name;

	/** the time of the event */
	long time;

	/**
	 * Constructor
	 * 
	 * @param wd the watch descriptor
	 * @param name the file that triggered the event
	 * @param time the time at which the event occurred
	 */
	RenamingContainer(WatchData wd, String name, long time) {
		super();
		this.wd = wd;
		this.name = name;
		this.time = time;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.getClass().getSimpleName() + " [wd=");
		builder.append(this.wd);
		builder.append(", time=");
		builder.append(this.time);
		builder.append(", name=");
		builder.append(this.name);
		builder.append("]");
		return builder.toString();
	}
}
