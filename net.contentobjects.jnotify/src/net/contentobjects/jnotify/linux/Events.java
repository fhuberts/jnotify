/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 * 
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * Copyright (c) 2011 - Ferry Huberts
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 * 
 ******************************************************************************
 * Authors : Omry Yadan
 *           Ferry Huberts, 2011
 * </pre>
 ******************************************************************************/

package net.contentobjects.jnotify.linux;

import java.util.LinkedList;
import java.util.List;

/**
 * Event Definitions
 */
class Events {
	/** a file was accessed */
	final static int IN_ACCESS = 0x00000001;

	/** a file was modified */
	final static int IN_MODIFY = 0x00000002;

	/** a file's metadata changed */
	final static int IN_ATTRIB = 0x00000004;

	/** a file that was opened for write was closed */
	final static int IN_CLOSE_WRITE = 0x00000008;

	/** a file that was opened for read was closed */
	final static int IN_CLOSE_NOWRITE = 0x00000010;

	/** a file was opened */
	final static int IN_OPEN = 0x00000020;

	/** a file was moved from the mentioned location */
	final static int IN_MOVED_FROM = 0x00000040;

	/** a file was moved to the mentioned location */
	final static int IN_MOVED_TO = 0x00000080;

	/** a file was created */
	final static int IN_CREATE = 0x00000100;

	/** a file was deleted */
	final static int IN_DELETE = 0x00000200;

	/** the watched file was deleted */
	final static int IN_DELETE_SELF = 0x00000400;

	/** the watched file was moved */
	final static int IN_MOVE_SELF = 0x00000800;

	/** the backing filesystem of the watched file was unmounted */
	final static int IN_UNMOUNT = 0x00002000;

	/** the event queue for the watched file overflowed */
	final static int IN_Q_OVERFLOW = 0x00004000;

	/** the watch was removed */
	final static int IN_IGNORED = 0x00008000;

	/** helper: a file was closed */
	final static int IN_CLOSE = (IN_CLOSE_WRITE | IN_CLOSE_NOWRITE);

	/** helper: a file was moved */
	final static int IN_MOVE = (IN_MOVED_FROM | IN_MOVED_TO);

	/** special: the event occurred against a directory */
	final static int IN_ISDIR = 0x40000000;

	/** special: the event is only sent once */
	final static int IN_ONESHOT = 0x80000000;

	/**
	 * All of the events<br>
	 * <br>
	 * We build the list by hand so that we can add flags in the future and not
	 * break backwards compatibility. Applications will only get the events that
	 * they originally wanted. Be sure to add new events here!
	 */
	final static int IN_ALL_EVENT = (IN_ACCESS | IN_MODIFY | IN_ATTRIB | IN_CLOSE_WRITE | IN_CLOSE_NOWRITE | IN_OPEN
			| IN_MOVED_FROM | IN_MOVED_TO | IN_CREATE | IN_DELETE | IN_DELETE_SELF | IN_MOVE_SELF | IN_UNMOUNT
			| IN_Q_OVERFLOW | IN_IGNORED);

	/**
	 * Convert a Linux event mask into a readable string of event names
	 * 
	 * @param linuxMask the mask
	 * @return the string representation
	 */
	static String getMaskDesc(int linuxMask) {
		List<String> s = new LinkedList<String>() {
			private static final long serialVersionUID = -8929421440547128245L;

			@Override
			public String toString() {
				if (size() == 0) {
					return "";
				}

				StringBuilder sb = new StringBuilder();
				sb.append(get(0));

				int i = 1;

				while (i < size()) {
					sb.append('|');
					sb.append(get(i));
					i++;
				}

				return sb.toString();
			}
		};

		if ((linuxMask & Events.IN_ACCESS) != 0)
			s.add("IN_ACCESS");
		if ((linuxMask & Events.IN_MODIFY) != 0)
			s.add("IN_MODIFY");
		if ((linuxMask & Events.IN_ATTRIB) != 0)
			s.add("IN_ATTRIB");
		if ((linuxMask & Events.IN_CLOSE_WRITE) != 0)
			s.add("IN_CLOSE_WRITE");
		if ((linuxMask & Events.IN_CLOSE_NOWRITE) != 0)
			s.add("IN_CLOSE_NOWRITE");
		if ((linuxMask & Events.IN_OPEN) != 0)
			s.add("IN_OPEN");
		if ((linuxMask & Events.IN_MOVED_FROM) != 0)
			s.add("IN_MOVED_FROM");
		if ((linuxMask & Events.IN_MOVED_TO) != 0)
			s.add("IN_MOVED_TO");
		if ((linuxMask & Events.IN_CREATE) != 0)
			s.add("IN_CREATE");
		if ((linuxMask & Events.IN_DELETE) != 0)
			s.add("IN_DELETE");
		if ((linuxMask & Events.IN_DELETE_SELF) != 0)
			s.add("IN_DELETE_SELF");
		if ((linuxMask & Events.IN_MOVE_SELF) != 0)
			s.add("IN_MOVE_SELF");
		if ((linuxMask & Events.IN_UNMOUNT) != 0)
			s.add("IN_UNMOUNT");
		if ((linuxMask & Events.IN_Q_OVERFLOW) != 0)
			s.add("IN_Q_OVERFLOW");
		if ((linuxMask & Events.IN_IGNORED) != 0)
			s.add("IN_IGNORED");
		if ((linuxMask & Events.IN_ISDIR) != 0)
			s.add("IN_ISDIR");
		if ((linuxMask & Events.IN_ONESHOT) != 0)
			s.add("IN_ONESHOT");

		return s.toString();
	}
}
