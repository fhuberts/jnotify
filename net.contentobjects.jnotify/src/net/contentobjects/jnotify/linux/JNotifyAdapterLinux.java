/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 * 
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * Copyright (c) 2011 - Ferry Huberts
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 * 
 ******************************************************************************
 * Authors : Omry Yadan
 *           Ferry Huberts, 2011
 * </pre>
 ******************************************************************************/

package net.contentobjects.jnotify.linux;

import java.io.File;
import java.io.FilenameFilter;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.contentobjects.jnotify.JNotify;
import net.contentobjects.jnotify.JNotifyErrorCode;
import net.contentobjects.jnotify.JNotifyException;
import net.contentobjects.jnotify.JNotifyListener;
import net.contentobjects.jnotify.impl.Config;
import net.contentobjects.jnotify.impl.JNotifyAdapter;
import net.contentobjects.jnotify.impl.Util;

/**
 * The JNotify adapter for Linux
 */
public class JNotifyAdapterLinux implements JNotifyAdapter {
	private Logger logger = Logger.getLogger(this.getClass().getName());

	/*
	 * Administration
	 */

	/** a map of watch file descriptors against their watch data */
	private Map<Integer, WatchData> _linuxWd2WatchData = new TreeMap<Integer, WatchData>();

	/** a set of files that were added by registerToSubTree (auto-watches) */
	private Set<String> _autoWatchesPaths = new TreeSet<String>();

	/**
	 * Add a watch to the administration
	 * 
	 * @param wd the watch descriptor
	 * @param watchData the watch data
	 * @param parentWatchData the parent watch data, used for subtree watching
	 */
	private void addToAdministration(int wd, WatchData watchData, WatchData parentWatchData) {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					new Object[] {Integer.valueOf(wd), watchData, parentWatchData});
		}

		_linuxWd2WatchData.put(Integer.valueOf(wd), watchData);

		if (!watchData._user) {
			_autoWatchesPaths.add(watchData._path);
		}

		if (parentWatchData != null) {
			parentWatchData.addSubwatch(wd);
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/**
	 * Remove a watch from the administration
	 * 
	 * @param wd the watch descriptor
	 * @return the watch data for the watch descriptor, or null when the watch
	 *         descriptor was not present in the administration
	 */
	private WatchData removeFromAdministration(Integer wd) {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(), wd);
		}

		WatchData watchData = _linuxWd2WatchData.remove(wd);

		if (watchData != null) {
			if (!watchData._user) {
				_autoWatchesPaths.remove(watchData._path);
			}

			if (watchData._parentWatchData != null) {
				watchData._parentWatchData.removeSubwatch(watchData._linuxWd);
			}
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(), watchData);
		}

		return watchData;
	}

	/*
	 * Watch creation
	 */

	/**
	 * Create a watch: instruct the native code to create a watch and then add it
	 * to the administration
	 * 
	 * @param parentWatchData the watch data of the parent in case of subtree
	 *        watching, null otherwise
	 * @param user true when the watch is user-created, false when it is
	 *        automatically created (because of subtree watching)
	 * @param path the file to watch
	 * @param mask the mask of events to watch (JNotify events)
	 * @param linuxMask the mask of events to watch (Events events)
	 * @param watchSubtree true to do subtree watching
	 * @param listener the listener that receives notifications of the requested
	 *        watch events
	 * @return the watch data of the created watch
	 * @throws JNotifyException upon error
	 */
	private WatchData createWatch(WatchData parentWatchData, boolean user, File path, int mask, int linuxMask,
			boolean watchSubtree, JNotifyListener listener) throws JNotifyException {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(
					this.getClass().getName(),
					Thread.currentThread().getStackTrace()[1].getMethodName(),
					new Object[] {parentWatchData, Boolean.valueOf(user), path, Util.getMaskDesc(mask),
							Events.getMaskDesc(linuxMask), Boolean.valueOf(watchSubtree), listener});
		}

		WatchData watchData;
		String absPath = path.getAbsolutePath();
		synchronized (_linuxWd2WatchData) {
			int wd = jnotify_linux.addWatch(absPath, linuxMask);
			watchData = new WatchData(parentWatchData, user, absPath, wd, mask, linuxMask, watchSubtree, listener);
			addToAdministration(wd, watchData, parentWatchData);
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(), watchData);
		}

		return watchData;
	}

	/**
	 * Setup subtree watching.
	 * 
	 * @param isRoot true when path is the root of a watch
	 * @param parentWatch the parent watch data
	 * @param root the file to watch
	 * @param fireCreatedEvents true to send a create event of the root basename
	 *        to the parent watch
	 * @throws JNotifyException upon error
	 */
	private void registerToSubTree(boolean isRoot, WatchData parentWatch, File root, boolean fireCreatedEvents)
			throws JNotifyException {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					new Object[] {Boolean.valueOf(isRoot), parentWatch, root, Boolean.valueOf(fireCreatedEvents)});
		}

		assert (parentWatch._user);

		/*
		 * make sure user really requested to be notified on this event. (in case of
		 * recursive listening, this IN_CREATE flag is always on, even if the user
		 * is not interested in creation events)
		 */
		if (fireCreatedEvents && (parentWatch._mask & JNotify.FILE_CREATED) != 0) {
			String name = root.getAbsolutePath().substring(parentWatch._path.length() + 1);
			parentWatch.notifyFileCreated(name);
		}

		if (root.isDirectory()) {
			/* root was already registered by the calling method */
			if (!isRoot) {
				try {
					createWatch(parentWatch, false, root, parentWatch._mask, parentWatch._linuxMask, parentWatch._watchSubtree,
							parentWatch._listener);
				} catch (JNotifyException e) {
					if (e.getErrorCode() == JNotifyErrorCode.WATCH_LIMIT_REACHED) {
						logger.log(Level.WARNING,
								"registerToSubTree : warning, failed to register " + root + " :" + e.getMessage(), e);
						throw e;
					}
					/* else, on any other error, try subtree anyway.. */
				}
			}

			/* list all directories */
			String files[] = root.list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return new File(dir, name).isDirectory();
				}
			});
			if (files != null) {
				for (String file : files) {
					registerToSubTree(false, parentWatch, new File(root, file), fireCreatedEvents);
				}
			}
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/**
	 * Add a watch.
	 * 
	 * @param path the file path to watch
	 * @param mask the mask of events to watch (JNotify events)
	 * @param watchSubtree true to do subtree watching
	 * @param listener the listener that receives notifications of the requested
	 *        watch events
	 * @return the watch descriptor
	 * @throws JNotifyException upon error
	 */
	@Override
	public int addWatch(String path, int mask, boolean watchSubtree, JNotifyListener listener) throws JNotifyException {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					new Object[] {path, Util.getMaskDesc(mask), Boolean.valueOf(watchSubtree), listener});
		}

		if (jnotify_linux == null) {
			throw new IllegalStateException("Not started");
		}

		/* map mask to linux inotify mask. */
		int linuxMask = 0;
		if ((mask & JNotify.FILE_CREATED) != 0) {
			linuxMask |= Events.IN_CREATE;
		}
		if ((mask & JNotify.FILE_DELETED) != 0) {
			linuxMask |= Events.IN_DELETE;
			linuxMask |= Events.IN_DELETE_SELF;
		}
		if ((mask & JNotify.FILE_MODIFIED) != 0) {
			linuxMask |= Events.IN_MODIFY;
			linuxMask |= Events.IN_ATTRIB;
		}
		if ((mask & JNotify.FILE_RENAMED) != 0) {
			linuxMask |= Events.IN_MOVED_FROM;
			linuxMask |= Events.IN_MOVED_TO;
		}

		/*
		 * if watching subdirs, listen on create anyway. to know when new sub
		 * directories are created. these events should not reach the client code
		 */
		if (watchSubtree) {
			linuxMask |= Events.IN_CREATE;
		}

		/* create the 'root/user' watch */
		WatchData watchData = createWatch(null, true, new File(path), mask, linuxMask, watchSubtree, listener);
		if (watchSubtree) {
			try {
				File file = new File(path);
				registerToSubTree(true, watchData, file, false);
			} catch (JNotifyException e) {
				removeWatch(watchData._linuxWd);
				throw e;
			}
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					Integer.valueOf(watchData._linuxWd));
		}

		return watchData._linuxWd;
	}

	/*
	 * Watch removal
	 */

	/**
	 * Remove a watch: remove it from the administration and the actually remove
	 * the watch
	 * 
	 * @param wd the watch descriptor to remove
	 * @return true when the watch is successfully removed
	 * @throws JNotifyException upon error
	 */
	@Override
	public boolean removeWatch(int wd) throws JNotifyException {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					new Object[] {Integer.valueOf(wd)});
		}

		if (jnotify_linux == null) {
			throw new IllegalStateException("Not started");
		}

		synchronized (_linuxWd2WatchData) {
			List<String> errors = new LinkedList<String>() {
				private static final long serialVersionUID = 3647678084437220130L;

				@Override
				public String toString() {
					StringBuilder s = new StringBuilder();
					int index = size() - 1;
					while (index >= 0) {
						s.append(get(index));
						if (index != 0) {
							s.append("\n");
						}
						index--;
					}
					return s.toString();
				}
			};
			WatchData watchData = removeFromAdministration(Integer.valueOf(wd));
			if (watchData == null) {
				if (logger.isLoggable(Level.FINER)) {
					logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
							Boolean.FALSE);
				}

				return false;
			}

			try {
				jnotify_linux.removeWatch(watchData._linuxWd);
			} catch (JNotifyException e) {
				errors.add(e.getMessage());
			}

			if (watchData._user && (watchData._subWd != null)) {
				while (!watchData._subWd.isEmpty()) {
					Integer subwd = watchData._subWd.get(0);
					removeFromAdministration(subwd);
					try {
						jnotify_linux.removeWatch(subwd.intValue());
					} catch (JNotifyException e) {
						errors.add(e.getMessage());
					}
				}
			}
			if (!errors.isEmpty()) {
				throw new JNotifyException_linux(errors.toString(), 0);
			}

			if (logger.isLoggable(Level.FINER)) {
				logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
						Boolean.TRUE);
			}

			return true;
		}
	}

	/*
	 * Event Notifications
	 */

	/**
	 * Called by the listener
	 * 
	 * @param name the name of the file that triggered the events
	 * @param linuxWd the watch descriptor
	 * @param linuxMask the mask of events
	 * @param cookie a unique cookie, used to correlate events (moves)
	 * @see INotifyListener
	 */
	private void notifyChangeEvent(String name, int linuxWd, int linuxMask, int cookie) {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName(),
					new Object[] {name, Integer.valueOf(linuxWd), Events.getMaskDesc(linuxMask), Integer.valueOf(cookie)});
		}

		synchronized (_linuxWd2WatchData) {
			WatchData watchData = _linuxWd2WatchData.get(Integer.valueOf(linuxWd));

			if (logger.isLoggable(Level.FINER)) {
				logger.finer("watchData=" + watchData);
			}

			if (watchData != null) {
				/*
				 * when a watch is removed then the administration must be gone at this
				 * point. assert that we are not receiving an ignore event (watch
				 * removal) while there is administration present
				 */
				assert ((linuxMask & Events.IN_IGNORED) == 0);

				/* a single notification can carry multiple events */

				/* IN_CREATE */
				if ((linuxMask & Events.IN_CREATE) != 0) {
					if (watchData._watchSubtree) {
						File newRootFile = new File(watchData._path, name);
						try {
							/* fire events for newly found directories under the new root */
							registerToSubTree(false, WatchData.getParentWatch(watchData), newRootFile, true);
						} catch (JNotifyException e) {
							/*
							 * ignore missing files while registering subtree, may have
							 * already been deleted
							 */
							if (e.getErrorCode() != JNotifyErrorCode.NO_SUCH_FILE_OR_DIRECTORY)
								logger.log(Level.WARNING,
										"registerToSubTree : warning, failed to register " + newRootFile + " :" + e.getMessage()
												+ " code = " + e.getErrorCode(), e);
						}
					} else {
						watchData.notifyFileCreated(name);
					}
				}

				/* IN_MODIFY || IN_ATTRIB */
				if ((linuxMask & Events.IN_MODIFY) != 0 || ((linuxMask & Events.IN_ATTRIB) != 0)) {
					watchData.notifyFileModified(name);
				}

				/* IN_ATTRIB || IN_ATTRIB_SELF */
				if (((linuxMask & Events.IN_DELETE) != 0) || ((linuxMask & Events.IN_DELETE_SELF) != 0)) {
					watchData.notifyFileDeleted(name);
				}

				/* IN_MOVED_FROM */
				if ((linuxMask & Events.IN_MOVED_FROM) != 0) {
					renamingHandler.add(cookie, new RenamingContainer(watchData, name, System.currentTimeMillis()));
				}

				/* IN_MOVED_TO */
				if ((linuxMask & Events.IN_MOVED_TO) != 0) {
					RenamingContainer rc = renamingHandler.remove(cookie);
					watchData.notifyFileRenamed(name, rc);
				}
			} else if ((linuxMask & Events.IN_IGNORED) != 0) {
				/*
				 * when a watch is removed then the administration must be gone at this
				 * point. there is not watch data and we received an ignore event (watch
				 * removal) so everything is fine.
				 */
			} else {
				if (logger.isLoggable(Level.WARNING)) {
					logger.log(Level.WARNING, "Received an event but could not find wd " + linuxWd + ", ignoring...");
				}
			}
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	/*
	 * Main
	 */

	/** instance of the interface towards the native code */
	private JNotify_linux jnotify_linux = null;

	/** instance of the rename handler */
	private RenamingHandler renamingHandler = null;

	@Override
	public void activate(Config config) throws JNotifyException {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}

		if (jnotify_linux != null) {
			return;
		}

		synchronized (_linuxWd2WatchData) {
			_linuxWd2WatchData.clear();
			_autoWatchesPaths.clear();
		}

		RenamingHandler renamingHandlerLocal = new RenamingHandler();
		if (config != null) {
			renamingHandlerLocal.setScrubPeriod(config.scrubPeriod());
		}

		JNotify_linux jnotify_linuxLocal = new JNotify_linux();

		INotifyListener listener = new INotifyListener() {
			@Override
			public void notify(String name, int wd, int mask, int cookie) {
				try {
					notifyChangeEvent(name, wd, mask, cookie);
				} catch (Throwable e) {
					logger.log(Level.SEVERE, e.getMessage(), e);
				}
			}
		};

		jnotify_linuxLocal.start(listener);
		renamingHandlerLocal.start();

		renamingHandler = renamingHandlerLocal;
		jnotify_linux = jnotify_linuxLocal;

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}

	@Override
	public void deactivate() {
		if (logger.isLoggable(Level.FINER)) {
			logger.entering(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}

		if (jnotify_linux == null) {
			return;
		}

		renamingHandler.stop();
		renamingHandler = null;

		while (!_linuxWd2WatchData.isEmpty()) {
			Integer wd = _linuxWd2WatchData.keySet().toArray(new Integer[0])[0];
			boolean result = false;
			JNotifyException ex = null;
			try {
				result = removeWatch(wd.intValue());
			} catch (JNotifyException e) {
				result = false;
				ex = e;
			}
			if (!result) {
				logger.log(Level.WARNING, "Could not remove watch wd " + wd + ((ex != null) ? ": " + ex.getMessage() : ""), ex);
			}
		}

		jnotify_linux.stop();
		jnotify_linux = null;

		synchronized (_linuxWd2WatchData) {
			_autoWatchesPaths.clear();
			_linuxWd2WatchData.clear();
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.exiting(this.getClass().getName(), Thread.currentThread().getStackTrace()[1].getMethodName());
		}
	}
}
