/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 * 
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * Copyright (c) 2011 - Ferry Huberts
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 * 
 ******************************************************************************
 * Authors : Omry Yadan
 *           Ferry Huberts, 2011
 * </pre>
 ******************************************************************************/

package net.contentobjects.jnotify;

/**
 * The JNotify error code
 */
public enum JNotifyErrorCode {
	/** an unknown error occurred */
	UNSPECIFIED,

	/** no more watches can be added / memory exhausted */
	WATCH_LIMIT_REACHED,

	/** permission is denied */
	PERMISSION_DENIED,

	/** the specified path does not exist */
	NO_SUCH_FILE_OR_DIRECTORY
}
