/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 * 
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * Copyright (c) 2011 - Ferry Huberts
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 * 
 ******************************************************************************
 * Authors : Omry Yadan
 *           Ferry Huberts, 2011
 * </pre>
 ******************************************************************************/

package net.contentobjects.jnotify.impl;

import java.util.LinkedList;
import java.util.List;

import net.contentobjects.jnotify.JNotify;

/**
 * A utility class
 */
public class Util {
	/**
	 * Convert an event mask into a readable string
	 * 
	 * @param mask the event mask
	 * @return the string representation
	 */
	public static String getMaskDesc(int mask) {
		List<String> s = new LinkedList<String>() {
			private static final long serialVersionUID = -8929421440547128245L;

			@Override
			public String toString() {
				if (size() == 0) {
					return "";
				}

				StringBuilder sb = new StringBuilder();
				sb.append(get(0));

				int i = 1;

				while (i < size()) {
					sb.append('|');
					sb.append(get(i));
					i++;
				}

				return sb.toString();
			}
		};

		if ((mask & JNotify.FILE_CREATED) != 0)
			s.add("FILE_CREATED");
		if ((mask & JNotify.FILE_DELETED) != 0)
			s.add("FILE_DELETED");
		if ((mask & JNotify.FILE_MODIFIED) != 0)
			s.add("FILE_MODIFIED");
		if ((mask & JNotify.FILE_RENAMED) != 0)
			s.add("FILE_RENAMED");

		return s.toString();
	}
}
