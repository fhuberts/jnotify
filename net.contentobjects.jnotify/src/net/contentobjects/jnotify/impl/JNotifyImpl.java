/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 * 
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * Copyright (c) 2011 - Ferry Huberts
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 * 
 ******************************************************************************
 * Authors : Omry Yadan
 *           Ferry Huberts, 2011
 * </pre>
 ******************************************************************************/

package net.contentobjects.jnotify.impl;

import java.util.Map;

import net.contentobjects.jnotify.JNotify;
import net.contentobjects.jnotify.JNotifyException;
import net.contentobjects.jnotify.JNotifyListener;
import net.contentobjects.jnotify.linux.JNotifyAdapterLinux;
import aQute.bnd.annotation.component.Activate;
import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.ConfigurationPolicy;
import aQute.bnd.annotation.component.Deactivate;
import aQute.bnd.annotation.metatype.Configurable;

/**
 * Implementation of the JNotify interface. Takes care of the OS abstraction: it
 * creates the correct adapter on activation and removes it again on
 * deactivation.
 */
@Component(configurationPolicy = ConfigurationPolicy.optional, designate = Config.class)
public class JNotifyImpl implements JNotify {
	/** the OS specific adapter */
	private JNotifyAdapter adapter = null;

	/**
	 * Create an OS specific adapter and activate it. When activate was already
	 * called, then nothing is done.
	 * 
	 * @param config the configuration
	 * @throws JNotifyException upon error
	 */
	public void activateInternal(Config config) throws JNotifyException {
		if (adapter != null) {
			return;
		}

		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.equals("linux")) {
			adapter = new JNotifyAdapterLinux();
		} else {
			throw new RuntimeException("Unsupported OS : " + osName);
		}

		try {
			adapter.activate(config);
		} catch (JNotifyException e) {
			deactivate();
			throw e;
		}
	}

	/**
	 * OSGi variant of the activate method.
	 * 
	 * @param props configuration properties map
	 * @throws JNotifyException upon error
	 */
	@Activate
	public void activateOsgi(Map<String, Object> props) throws JNotifyException {
		activateInternal(Configurable.createConfigurable(Config.class, props));
	}

	/**
	 * Activate the library: create an OS specific adapter and activate it. When
	 * activate was already called, then nothing is done.
	 * 
	 * @throws JNotifyException upon error
	 */
	public void activate() throws JNotifyException {
		activateInternal(null);
	}

	/**
	 * Deactivates the OS specific adapter and frees it. When deactivate was
	 * already called, then nothing is done.
	 */
	@Deactivate
	public void deactivate() {
		if (adapter == null) {
			return;
		}

		try {
			adapter.deactivate();
		} finally {
			adapter = null;
		}
	}

	@Override
	public int addWatch(String path, int mask, boolean watchSubtree, JNotifyListener listener) throws JNotifyException {
		if (adapter == null) {
			return -1;
		}

		return adapter.addWatch(path, mask, watchSubtree, listener);
	}

	@Override
	public boolean removeWatch(int watchId) throws JNotifyException {
		if (adapter == null) {
			return false;
		}

		return adapter.removeWatch(watchId);
	}
}
