package net.contentobjects.jnotify.impl;

import aQute.bnd.annotation.metatype.Meta;

/** the configuration interface */
@Meta.OCD(name = "JNotify", description = "Configuration for the JNotify service")
public interface Config {
	@Meta.AD(min = "100", max = "36000000", deflt = "10000", required = false, description = "Rename handler scrub period (milliseconds). Only relevant for Linux.")
	long scrubPeriod();
}
