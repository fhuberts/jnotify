/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 * 
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * Copyright (c) 2011 - Ferry Huberts
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 * 
 ******************************************************************************
 * Authors : Omry Yadan
 *           Ferry Huberts, 2011
 * </pre>
 ******************************************************************************/

package net.contentobjects.jnotify;

import java.io.IOException;

/**
 * A base exception class for JNotify exceptions
 */
public abstract class JNotifyException extends IOException {
	private static final long serialVersionUID = -2099930506354069566L;

	/** the OS specific error code */
	protected final int errno;

	/**
	 * Constructor
	 * 
	 * @param s the message
	 * @param errno the error code
	 */
	public JNotifyException(String s, int errno) {
		super(s);
		this.errno = errno;
	}

	/**
	 * @return errno
	 */
	public int getErrno() {
		return errno;
	}

	/**
	 * Convert the OS specific error code into a JNotify error code
	 * 
	 * @return the JNotify error code
	 */
	public abstract JNotifyErrorCode getErrorCode();
}
