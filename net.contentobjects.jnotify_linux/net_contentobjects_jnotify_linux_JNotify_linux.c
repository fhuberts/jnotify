/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 *
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * Copyright (c) 2011 - Ferry Huberts
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 *
 ******************************************************************************
 * Authors : Omry Yadan
 *           Ferry Huberts, 2011
 * </pre>
 ******************************************************************************/

#include "net_contentobjects_jnotify_linux_JNotify_linux.h"
#include <sys/inotify.h>
#include <pthread.h>
#include <syslog.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>

/* called by the dynamic linker upon unloading the library */
void __attribute__ ((destructor)) destroy(void);

/*
 * Signals
 */

/**
 * Empty callback function. Used for the SIGUSR1 signal which is only used to
 * wake up the thread main loop.
 */
void emptySignalHandler(int rcvsignal) {
	return;
}

/**
 This function installs an empty signal handler for SIGUSR1 and then
 unblocks it

 @return errno
 */
int registerSignalHandler() {
	int result = 0;
	sigset_t signalSet;
	struct sigaction signalAction;

	/* first clear, then fill in the signal handler */
	memset(&signalAction, 0, sizeof(signalAction));
	signalAction.sa_handler = &emptySignalHandler;
	sigemptyset(&signalAction.sa_mask);
	signalAction.sa_flags = SA_RESTART;

	/* register the signal handler for SIGUSR1 */
	if (sigaction(SIGUSR1, &signalAction, NULL)) {
		result = errno;
		syslog(LOG_ERR, "Could not register signal handler for SIGUSR1: %s\n",
				strerror(errno));
		return result;
	}

	/* first clear, then add SIGUSR1 to the signal set */
	sigemptyset(&signalSet);
	if (sigaddset(&signalSet, SIGUSR1)) {
		result = errno;
		syslog(LOG_ERR, "Could not add SIGUSR1 to signal set: %s\n",
				strerror(errno));
		return result;
	}

	/* unblock SIGUSR1 */
	if (sigprocmask(SIG_UNBLOCK, &signalSet, NULL)) {
		result = errno;
		syslog(LOG_ERR, "Could not unblock SIGUSR1: %s\n", strerror(errno));
		return result;
	}

	return 0;
}

/**
 * inotify fd.
 */

#define BUFFER_SIZE 4096

static const char * syslogName = "JNotify";
static const char * callbackEventMethod = "callbackProcessEvent";
static const char * callbackRunningMethod = "callbackRunning";

int fd = -1;
bool run = true;
pthread_t threadId = 0;

char eventBuffer[BUFFER_SIZE];

void destroy() {
	if (fd != -1) {
		close(fd);
		fd = -1;
	}
}

void dispatch(JNIEnv *env, jobject this, struct inotify_event *event) {
	jstring name;
	if (event->len) {
		name = (*env)->NewStringUTF(env, event->name);
	} else {
		name = (*env)->NewStringUTF(env, "");
	}

	/* find method: void callbackProcessEvent(String name, int wd, int mask, int cookie) */
	jclass clazz = (*env)->GetObjectClass(env, this);
	jmethodID mid = (*env)->GetMethodID(env, clazz, callbackEventMethod,
			"(Ljava/lang/String;III)V");
	if (mid == NULL) {
		syslog(LOG_ERR, "Method %s not found", callbackEventMethod);
		return;
	}

	(*env)->CallVoidMethod(env, this, mid, name, event->wd, event->mask,
			event->cookie);

	/* we need to delete name locally or Java will hold it until the thread exits */
	(*env)->DeleteLocalRef(env, name);
}

void callbackRunning(JNIEnv *env, jobject this, jboolean running) {
	/* find method: void callbackRunning(boolean running) */
	jclass clazz = (*env)->GetObjectClass(env, this);
	jmethodID mid = (*env)->GetMethodID(env, clazz, callbackRunningMethod,
			"(Z)V");
	if (mid == NULL) {
		syslog(LOG_ERR, "Method %s not found", callbackRunningMethod);
		return;
	}

	(*env)->CallVoidMethod(env, this, mid, running);
}

/**
 * @return errno
 */
int runLoop(JNIEnv *env, jobject this) {
	int result = 0;

	run = true;

	openlog(syslogName, LOG_PID, LOG_USER);

	if (fd != -1) {
		syslog(LOG_WARNING, "Already started, not starting again");
		result = EPERM;
		goto end;
	}

	threadId = pthread_self();

	result = registerSignalHandler();
	if (result) {
		goto end;
	}

	fd = inotify_init();
	if (fd == -1) {
		result = errno;
		syslog(LOG_ERR, "Failed to initialise inotify: %s", strerror(errno));
		goto end;
	}

	callbackRunning(env, this, JNI_TRUE);

	ssize_t len = 0;
	ssize_t i = 0;

	while (run) {
		while (i < len) {
			struct inotify_event *event =
					(struct inotify_event *) &eventBuffer[i];
			dispatch(env, this, event);
			i += sizeof(struct inotify_event) + event->len;
		}

		i = 0;
		len = read(fd, eventBuffer, BUFFER_SIZE);
	}

	if (close(fd) == -1) {
		syslog(LOG_ERR, "Failed to shutdown inotify (ignored): %s",
				strerror(errno));
	}
	fd = -1;

	result = 0;

	end: callbackRunning(env, this, JNI_FALSE);

	closelog();

	run = false;

	return result;
}

/*
 * Class:     net_contentobjects_jnotify_linux_JNotify_linux
 * Method:    nativeStop
 * Signature: ()I
 *
 * @return errno
 */
JNIEXPORT jint JNICALL Java_net_contentobjects_jnotify_linux_JNotify_1linux_nativeStop(
		JNIEnv *env, jobject this) {
	run = false;
	if (fd != -1) {
		return (jint) pthread_kill(threadId, SIGUSR1);
	}

	return (jint) 0;
}

/*
 * Class:     net_contentobjects_jnotify_linux_JNotify_linux
 * Method:    nativeAddWatch
 * Signature: (Ljava/lang/String;I)I
 *
 * @return -errno on error, or the watch descriptor otherwise
 */
JNIEXPORT jint JNICALL Java_net_contentobjects_jnotify_linux_JNotify_1linux_nativeAddWatch(
		JNIEnv *env, jobject this, jstring path, jint mask) {
	const char * str = (*env)->GetStringUTFChars(env, path, NULL);
	if (str == NULL) {
		return -ENOMEM;
	}

	int wd = inotify_add_watch(fd, (char*) str, mask);
	if (wd == -1) {
		wd = -errno;
		syslog(LOG_ERR, "Failed to add a watch on %s: %s", (char*) str,
				strerror(errno));
	}

	(*env)->ReleaseStringUTFChars(env, path, str);

	return wd;
}

/*
 * Class:     net_contentobjects_jnotify_linux_JNotify_linux
 * Method:    nativeRemoveWatch
 * Signature: (I)I
 *
 * @return errno
 */
JNIEXPORT jint JNICALL Java_net_contentobjects_jnotify_linux_JNotify_1linux_nativeRemoveWatch(
		JNIEnv *jni, jobject this, jint wd) {
	int ret = inotify_rm_watch(fd, wd);
	if (ret == -1) {
		ret = errno;
		syslog(LOG_ERR, "Failed to remove watch %d: %s", wd, strerror(errno));
	}

	return ret;
}

/*
 * Class:     net_contentobjects_jnotify_linux_JNotify_linux
 * Method:    nativeNotifyLoop
 * Signature: ()I
 *
 * @return errno
 */
JNIEXPORT jint JNICALL Java_net_contentobjects_jnotify_linux_JNotify_1linux_nativeNotifyLoop(
		JNIEnv *env, jobject this) {
	return runLoop(env, this);
}

/*
 * Class:     net_contentobjects_jnotify_linux_JNotify_linux
 * Method:    getErrorDesc
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_net_contentobjects_jnotify_linux_JNotify_1linux_getStrError(
		JNIEnv *env, jobject this, jint errNo) {
	return (*env)->NewStringUTF(env, strerror(errNo));
}
