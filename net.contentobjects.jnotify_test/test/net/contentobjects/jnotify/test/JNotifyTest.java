/*******************************************************************************
 * JNotify - Allow java applications to register to File system events.
 * 
 * <pre>
 * Copyright (C) 2005 - Content Objects
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************
 *
 * Content Objects, Inc., hereby disclaims all copyright interest in the
 * library `JNotify' (a Java library for file system events).
 * 
 * Yahali Sherman, 21 November 2005
 *    Content Objects, VP R&D.
 * 
 ******************************************************************************
 * Author : Omry Yadan
 * </pre>
 ******************************************************************************/

package net.contentobjects.jnotify.test;

import java.io.IOException;

import net.contentobjects.jnotify.JNotify;
import net.contentobjects.jnotify.JNotifyException;
import net.contentobjects.jnotify.JNotifyListener;
import net.contentobjects.jnotify.impl.JNotifyImpl;

public class JNotifyTest {

	/**
	 * @param args
	 * @throws JNotifyException 
	 * @throws IOException
	 */
	public static void main(String[] args) throws JNotifyException {
		JNotifyImpl impl = new JNotifyImpl();
		impl.activate();
		try {
			// to add a watch :
			String path = System.getProperty("user.home");
			int mask = JNotify.FILE_CREATED | JNotify.FILE_DELETED
					| JNotify.FILE_MODIFIED | JNotify.FILE_RENAMED;
			boolean watchSubtree = true;
			System.err.println("Adding a watch on " + path);
			int watchID = impl.addWatch(path, mask, watchSubtree,
					new JNotifyListener() {
						public void fileRenamed(int wd, String rootPath,
								String oldName, String newName) {
							System.out
									.println("JNotifyTest.fileRenamed() : wd #"
											+ wd + " root = " + rootPath + ", "
											+ oldName + " -> " + newName);
						}

						public void fileModified(int wd, String rootPath,
								String name) {
							System.out
									.println("JNotifyTest.fileModified() : wd #"
											+ wd
											+ " root = "
											+ rootPath
											+ ", "
											+ name);
						}

						public void fileDeleted(int wd, String rootPath,
								String name) {
							System.out
									.println("JNotifyTest.fileDeleted() : wd #"
											+ wd + " root = " + rootPath + ", "
											+ name);
						}

						public void fileCreated(int wd, String rootPath,
								String name) {
							System.out
									.println("JNotifyTest.fileCreated() : wd #"
											+ wd + " root = " + rootPath + ", "
											+ name);
						}
					});

			System.err.println("done");

			try {
				Thread.sleep(2000000);
			} catch (InterruptedException e1) {
			}

			// to remove watch:
			boolean res = impl.removeWatch(watchID);
			if (!res) {
				// invalid watch ID specified.
			}

		} catch (JNotifyException e) {
			switch (e.getErrorCode()) {
			case NO_SUCH_FILE_OR_DIRECTORY:
				System.err
						.println("JNotifyException.ERROR_NO_SUCH_FILE_OR_DIRECTORY");
				break;
			case PERMISSION_DENIED:
				System.err.println("JNotifyException.ERROR_PERMISSION_DENIED");
				break;
			case WATCH_LIMIT_REACHED:
				System.err
						.println("JNotifyException.ERROR_WATCH_LIMIT_REACHED");
				break;
			case UNSPECIFIED:
				System.err.println("JNotifyException.ERROR_UNSPECIFIED");
				break;
			default:
				break;
			}
			e.printStackTrace();

		} finally {
			impl.deactivate();
		}
	}

}
